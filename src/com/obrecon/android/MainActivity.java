package com.obrecon.android;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.helloandroid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private static final int REQUEST_IMAGE_CAPTURE = 1;
	private static final int REQUEST_IMAGE_SELECTION = 2;
	private ImageView imgView;
	private Bitmap bitmap;
	private ProgressDialog progressDialog;
	Uri imageUri;
	Button compareBtn;
	MainActivity mainAct;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		imgView = (ImageView) findViewById(R.id.imgPreview);

		Button captureBtn = (Button) findViewById(R.id.btnCapturar);
		setBtnListenerOrDisable(captureBtn, mTakePicOnClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE);

		Button selectBtn = (Button) findViewById(R.id.btnSeleccionar);
		setBtnListenerOrDisable(selectBtn, mSelectPicOnClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE);

		compareBtn = (Button) findViewById(R.id.btnComparar);
		compareBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if (bitmap == null) {
					Toast.makeText(getApplicationContext(),
							"Please select image", Toast.LENGTH_SHORT).show();
				} else {
					progressDialog = ProgressDialog.show(MainActivity.this,
							"Uploading", "Please wait...", true);
					new ImageUploadTask().execute();
				}
			}
		});

		mainAct = this;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	Button.OnClickListener mTakePicOnClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			dispatchTakePictureIntent();
		}
	};

	Button.OnClickListener mSelectPicOnClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			dispatchSelectPictureIntent();
		}
	};

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			// define the file name for the photo taken by the camera activity
			String fileName = "new-photo-name.jpg";
			// create parameters for the Intent with the file name
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, fileName);
			values.put(MediaStore.Images.Media.DESCRIPTION,
					"Imagen capturada por la c�mara");
			// imageUri is the current activity attribute, defined and saved for
			// later usage
			imageUri = getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}

	private void dispatchSelectPictureIntent() {
		Intent selectPictureIntent = new Intent();
		selectPictureIntent.setType("image/*");
		selectPictureIntent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(
				Intent.createChooser(selectPictureIntent, "Seleccionar Imagen"),
				REQUEST_IMAGE_SELECTION);
	}

	/**
	 * Indicates whether the specified action can be used as an intent. This
	 * method queries the package manager for installed packages that can
	 * respond to an intent with the specified action. If no suitable package is
	 * found, this method returns false.
	 * http://android-developers.blogspot.com/2009/01/can-i-use-this-intent.html
	 * 
	 * @param context
	 *            The application's environment.
	 * @param action
	 *            The Intent action to check for availability.
	 * 
	 * @return True if an Intent with the specified action can be sent and
	 *         responded to, false otherwise.
	 */
	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list = packageManager.queryIntentActivities(intent,
				PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	private void setBtnListenerOrDisable(Button btn,
			Button.OnClickListener onClickListener, String intentName) {
		if (isIntentAvailable(this, intentName)) {
			btn.setOnClickListener(onClickListener);
		} else {
			btn.setText(getText(R.string.cannot).toString() + " "
					+ btn.getText());
			btn.setClickable(false);
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Uri selectedImageUri = null;
		String filePath = null;
		switch (requestCode) {
		case REQUEST_IMAGE_CAPTURE:
			if (resultCode == RESULT_OK) {
				// use imageUri here to access the image
				selectedImageUri = imageUri;
			}
			break;
		case REQUEST_IMAGE_SELECTION:
			if (resultCode == Activity.RESULT_OK) {
				selectedImageUri = data.getData();
			}
			break;
		}
		if (selectedImageUri != null) {
			try {
				// OI FILE Manager
				String filemanagerstring = selectedImageUri.getPath();

				// MEDIA GALLERY
				String selectedImagePath = getPath(selectedImageUri);

				if (selectedImagePath != null) {
					filePath = selectedImagePath;
				} else if (filemanagerstring != null) {
					filePath = filemanagerstring;
				} else {
					Toast.makeText(getApplicationContext(), "Unknown path",
							Toast.LENGTH_LONG).show();
				}

				if (filePath != null) {
					startComparisonStep(filePath);
				} else {
					bitmap = null;
				}
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), "Internal error",
						Toast.LENGTH_LONG).show();
			}
		}
	}

	@SuppressWarnings("deprecation")
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		if (cursor != null) {
			// HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
			// THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} else
			return null;
	}

	public void startComparisonStep(String filePath) {
		decodeFile(filePath);
		compareBtn.setVisibility(View.VISIBLE);
	}

	public void decodeFile(String filePath) {
		// Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;

		// TODO: Troubleshoot. This is not working when the picture comes from
		// the camera.
		BitmapFactory.decodeFile(filePath, o);

		// The new size we want to scale to
		final int REQUIRED_SIZE = 640;

		// Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale = 1;
		while (true) {
			if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
				break;
			width_tmp /= 2;
			height_tmp /= 2;
			scale *= 2;
		}

		// Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		bitmap = BitmapFactory.decodeFile(filePath, o2);

		imgView.setImageBitmap(bitmap);
	}

	class ImageUploadTask extends AsyncTask<Void, Void, String> {

		@SuppressWarnings("unused")
		@Override
		protected String doInBackground(Void... unsued) {
			String result = "";
			InputStream is;
			BitmapFactory.Options bfo;
			Bitmap bitmapOrg;
			ByteArrayOutputStream bao;

			bfo = new BitmapFactory.Options();
			bfo.inSampleSize = 2;

			bao = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
			byte[] ba = bao.toByteArray();
			String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
			ArrayList nameValuePairs = new ArrayList();
			nameValuePairs.add(new BasicNameValuePair("imgBase64", ba1));
			Log.v("log_tag", System.currentTimeMillis() + ".jpg");
			try {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new
				// Here you need to put your server file address
				HttpPost("http://aspspider.info/polack/Home/CompareImage");
				// HttpPost("http://192.168.1.133/Obrecon/Home/CompareImage");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				result = convertInputStreamToString(is);
				Log.v("log_tag", "In the try Loop");
			} catch (Exception e) {
				Log.v("log_tag", "Error in http connection " + e.toString());
				result = "{ \"error\": \"Error en conexi�n HTTP\"}";
			}
			return result;
		}

		@Override
		protected void onProgressUpdate(Void... unsued) {

		}

		@Override
		protected void onPostExecute(String stringResponse) {
			try {
				if (progressDialog.isShowing())
					progressDialog.dismiss();
				AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mainAct);
				LayoutInflater inflater = getLayoutInflater();
				Context context = mainAct.getBaseContext();
				// Inflate and set the layout for the dialog
				View alertLayout = inflater.inflate(R.layout.result_dialog,
						new LinearLayout(context), false);

				JSONObject response = new JSONObject(stringResponse);
				JSONArray results = response.optJSONArray("results");
				if (results == null) {
					String errorMessage = response.getString("error");
					if (errorMessage != null) {
						TextView failureTxt = (TextView) alertLayout
								.findViewById(R.id.failureTxt);
						failureTxt.setText(errorMessage);
						failureTxt.setVisibility(View.VISIBLE);
					}
				} else if (results.length() > 0) {
					JSONObject topResult = results.getJSONObject(0);

					ImageView imgView = (ImageView) alertLayout
							.findViewById(R.id.resultImg);
					new DownloadImageTask(imgView).execute(topResult
							.getJSONObject("image").getString("thumb_120"));

					TextView successTxt = (TextView) alertLayout
							.findViewById(R.id.successTxt);
					if (results.length() == 1) {
						successTxt
								.setText(String
										.format("Hay un �tem reconocible, se llama %s y su puntaje es %s.",
												topResult.getJSONObject("item")
														.getString("name"),
												topResult.getString("score")));
					} else {
						successTxt
								.setText(String
										.format("Hay %s �tems reconocibles. Aquel con el puntaje m�s alto (%s) es '%s'.",
												results.length(), topResult
														.getString("score"),
												topResult.getJSONObject("item")
														.getString("name")));
					}

					LinearLayout successLyt = (LinearLayout) alertLayout
							.findViewById(R.id.successLyt);
					successLyt.setVisibility(View.VISIBLE);
				} else {
					TextView failureTxt = (TextView) alertLayout
							.findViewById(R.id.failureTxt);
					failureTxt.setVisibility(View.VISIBLE);
				}

				dlgAlert.setView(alertLayout);
				dlgAlert.setTitle("Resultado");
				dlgAlert.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				dlgAlert.setCancelable(true);
				dlgAlert.create().show();
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(), e.getMessage(),
						Toast.LENGTH_LONG).show();
				Log.e(e.getClass().getName(), e.getMessage(), e);
			}
		}

		private String convertInputStreamToString(InputStream inputStream)
				throws IOException {
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream));
			String line = "";
			String result = "";
			while ((line = bufferedReader.readLine()) != null)
				result += line;

			inputStream.close();
			return result;
		}
	}

	class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			bmImage.setImageBitmap(result);
		}
	}
}